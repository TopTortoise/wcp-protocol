import socket
import json

class WcpConnection:

    def __init__(self, addr='127.0.0.1', port=54321, verbose=False):
        self.verbose = verbose
        self.buffer = b''
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((addr, port))
        data = self.read_message()
        self.log(f'S>C: {data!r}')

    def log(self, *args):
        if self.verbose:
            print(*args)

    def send(self, message: str):
        message = json.dumps(message).encode() + b'\0'
        self.log(f'C>S: {message!r}')
        self.socket.send(message)

        # read response
        data = self.read_message()
        self.log(f'S>C: {data!r}')
        return data

    def read_message(self):
        raw_data = self.buffer
        self.buffer = b''
        acc = raw_data
        while b'\x00' not in raw_data:
            raw_data = self.socket.recv(1024)
            if not raw_data:
                return None

            acc += raw_data

        data = str(acc, 'ascii').split('\x00')
        if len(data) > 1 and data[1]:
            self.buffer = bytes('\x00'.join(data[1:]), 'ascii')

        return json.loads(data[0])
        
    def add_variables(self, variables: [str]):
        assert all(isinstance(var, str) for var in variables), 'Arguments must be strings'
        response = self.send({
            'type': 'command',
            'command': 'add_variables',
            'names': variables
        })

        if response['type'] == 'response':
            return response['arguments']
        else:
            print('Error:', response)

    def get_item_list(self):
        response = self.send({
            'type': 'command',
            'command': 'get_item_list',
        })

        if response['type'] == 'response':
            return response['arguments']
        else:
            print('Error:', response)

    def get_item_info(self, item_ids: [int | str]):
        assert all(isinstance(item_id, (int, str)) for item_id in item_ids), 'Arguments must be strings or integers'
        response = self.send({
            'type': 'command',
            'command': 'get_item_info',
            'ids': item_ids
        })

        return response

    def set_item_color(self, item_id: int | str, color_name: str):
        assert isinstance(item_id, (int, str))
        assert isinstance(color_name, str)

        self.send({
            'type': 'command',
            'command': 'set_item_color',
            'id': item_id,
            'color': color_name
        })

    def clear_items(self):
        self.send({
            'type': 'command',
            'command': 'clear_items'
        })

    def remove_items(self, item_ids: [int | str]):
        assert all(isinstance(item_id, (int, str)) for item_id in item_ids), 'Arguments must be strings or integers'

        self.send({
            'type': 'command',
            'command': 'remove_items'
        })
    