from wcp.wcp import WcpConnection

con = WcpConnection(verbose=False)
con.add_variables(['tb.clk', 'tb.reset'])
item_ids = con.get_item_list()
item_info = con.get_item_info(item_ids)
con.set_item_color(item_ids[-1], 'red')
con.set_item_color(item_ids[-2], 'yellow')

