# Waveform Viewer Control Protocol

## Definitions

```
    Item List
   +----------------+--------------------------------------+
   |Name        (ID)|                                      |
L I|Variable    (1) |___/----\________/----\________/----\_|
i t|Variable    (2) |---{0xff}-----------------------------|
s e|Divider     (3) |                                      |
t m|                |                                      |
e s|                |                                      |
d  |                |                                      |
   |Listed Item (n) |                                      |
   +-------------------------------------------------------+
```

ID: a unique identifier that identifies displayed items

Item: Variables, Dividers, Timelines, Markers, ...

Item List: The list containing the currently shown items.

Listed item: An item contained in the item list



### Commands:
Commands are sent as JSON objects. Each command or response is followed by a null byte. 


- get_item_list
  - returns a list IDs of currently listed items. Ordered as displayed in the waveform viewer.
  - are IDs always numbers? or strings better for general case?
  - Example
  ```
        C>S
        {
            "type": "command",
            "command": "get_item_list",
            "arguments": []
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "get_item_list",
          "results": [1, 5, 6]
        }
  ```
     
- get_item_info
    - returns a list of objects describing the current state of the requested listed items
    - arguments: a list of IDs
    
    ```
            C>S
            {
                "type": "command",
                "command": "get_item_info",
                "arguments": [1, 5, 6]
            }
    ```
    ```
            S>C
            {
                "type": "response",
                "command": "get_item_info",
                "results": [
                    {
                        "name": "tb.clk",
                        "type": "variable",
                        "id": "1"
                    },
                    {
                        "name": "tb.clk",
                        "type": "variable",
                        "id": "2"
                    },
                    {
                        "name": "",
                        "type": "divider",
                        "id": "3"
                    }
                ]
            }
     ```
- set_item_color(id, color_name)
  - sets the color of the specified item
  - arguments
    - id: an ID
    - color_name: a string with the name of the requested color 
  
  ```
        C>S
        {
          "type": "command",
          "command": "set_item_color",
          "id": 15,
          "color": "red"
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "set_item_color",
          "arguments": []
        }     
  ```  
        
- add_variables
  - adds variables [v_1, v_2, ..., v_n] to the item list
  - returns a list of IDs [id_1, id_2, ..., id_n] where id_1 belongs to v_1, id_n belongs to v_n and so on.
  ``` 

        C>S
        {
          "command": "add_variable",
          "type": "command",
          "arguments": [
            "TOP.clk",
            "TOP.clk"
          ]
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "add_variable",
          "arguments": [
            "0",
            "1"
          ]
        }
  ```
  
- add_scope(scope_name)
  - adds all variables from a scope
  - returns a list of (name, ID) tuples
  ```
        C>S
        {
          "command": "add_scope",
          "scopes": [
            "TOP"
          ]
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "add_scope",
          "arguments": [
            [
              "TOP.clock",
              1
            ],
            [
              "TOP.reset",
              2
            ]
          ]
        }
  ```

        
- Load (file_name)
  - loads the waveform 
        
- Reload
  - reloads current waveform(s?)
  ```
        C>S
        {
          "command": "reload"
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "reload",
          "arguments": []
        }
  ```
        
- set_viewport_to(timestamp)
  - moves view to timestamp
  ```
        C>S
        {
          "command": "goto",
          "timestamp": [
            1,
            [
              400
            ]
          ]
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "goto",
          "arguments": []
        }
  ```
- set_cursor(timestamp)
  - sets cursor to timestamp
  

- remove_item(ids)
  - removes items by ids
  ```
        C>S
        {
          "command": "remove_items",
          "ids": [
            1,
            2,
            3,
            4
          ]
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "remove_items",
          "arguments": []
        }
  ```
- focus_item(id)
  - focus item with id
  ```
        C>S
        {
          "command": "focus_item",
          "id": 1
        }
  ```
  ```
        S>C
        {
          "type": "response",
          "command": "focus_item",
          "arguments": [
            1
          ]
        }
  ```
- clear
  - clears all item on screen
  ```
      C>S
      {
        "command": "clear"
      }    
  ```
  ```
      S>C
      {
        "type": "response",
        "command": "clear",
        "arguments": []
      }
  ```
- screenshot(file_name, width, length, diff)
    - takes a screenshot of surfer and creates png file
        - the diff variable will create two new png files one of the normal screenshot and the other is the difference of the new screenshot and the old screenshot, if there exists a file with the same name and if the differences between the existing file and the new file are big enough.    
  ```
      C>S
      {
        "command": "screenshot",
        "file_name": "file_name",
        "width": 1280.0,
        "length": 720.0,
        "diff": true
      }
  ```
  ```
      S>C
      {
        "type": "response",
        "command": "screenshot",
        "arguments": []
      }
  ```

- load (file_name)
 - loads waveform file

  ```
      C>S
      {
        "command": "load",
        "file_name": "file_name"
      }
  ```
  ```
      S>C
      {
        "type": "response",
        "command": "load",
        "arguments": []
      }
  ```
- zoom_to_fit (viewport_idx)
    - zooms to fit of the given viewport
  ```
      C>S
      {
        "command": "zoom_to_fit",
        "viewport_idx": 0
      }
  ```
  ```
      S>C
      {
        "type": "response",
        "command": "zoom_to_fit",
        "arguments": []
      }
  ```
